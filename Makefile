.PHONY: build push

REPO_URL=registry.gitlab.com/alehed/gitlab-docker-raspberry-pi
RELEASE_VERSION = 16.2.1-ce.0
# Set to non-0 when building more than one image per version
BUILD = 0
RELEASE_TAG = "$(RELEASE_VERSION)-$(BUILD)"

build:
	# We only build from repos with no changes
	test -z "$$(git status -s)"

	# This will fail if the tag already exists
	git tag $(RELEASE_TAG)

	# This needs to run after every boot
	sudo podman run --rm --privileged multiarch/qemu-user-static --reset --persistent yes

	# For some reason we can't build as rootless podman
	sudo podman build --format=docker --arch=arm64 --build-arg RELEASE_PACKAGE=gitlab-ce --build-arg "RELEASE_VERSION=$(RELEASE_VERSION)" --tag=$(REPO_URL):$(RELEASE_TAG) --tag=$(REPO_URL):latest .

push: build
	sudo podman push $(REPO_URL):$(RELEASE_TAG)
	sudo podman push $(REPO_URL):latest
	git push origin $(RELEASE_TAG)
