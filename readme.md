# 64 Bit ARM Raspberry Pi Docker Image

Since GitLab version 13.4, 64 bit arm images are published by GitLab, but not
the corresponding docker image. Since I self-host GitLab on a Raspberry Pi 4, I
use this repo to build these arm64 images for the Raspberry Pi.

The images built from this repo are published at the GitLab docker registry of
this repo at `registry.gitlab.com/alehed/gitlab-docker-raspberry-pi`.

Security note: This is a third-party image, don't trust third-party images if
you are not the one who created them.

## Changes from the stock GitLab image

This is largely based on the x86 docker image with a few modifications:

* Assets are split into runtime and buildtime assets, this makes rebuilds where
  only the runtime assets change a lot faster
* A strict docker ignore file is added which also makes rebuilding faster
* The RELEASE file is replaced with build args for the docker file
* The gitlab.rb file contains some the Raspberry Pi optimizations from the
  GitLab docs

The upstream GitLab image can be found at:
<https://gitlab.com/gitlab-org/omnibus-gitlab/-/tree/master/docker>

This repo is currently up to date with upstream as of 5cae1797.

## Updating the image

### Prerequisites

Since I am on NixOS and prefer to use podman, these instructions assume podman
and Nix(OS). You should also be able to run this from pretty much any Linux OS
and using docker buildx though (probably with fewer difficulties).

### Every time

Whenever GitLab publishes a new image. What you need to do in order to build an
equivalent image for the Raspberry Pi is:

* Check for changes in the
  [upstream repo](https://gitlab.com/gitlab-org/omnibus-gitlab/-/commits/master/docker)
  and apply those.

* Update the hash in this readme and commit the changes.

* Change the `RELEASE_VERSION` in the `Makefile`.

* Run `make push`.
