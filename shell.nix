let
  sources = import ./nix/sources.nix;
  pkgs = import sources.nixpkgs {};
in
pkgs.mkShell {
  buildInputs = [
    pkgs.gnumake
    pkgs.podman
    pkgs.buildah
    pkgs.runc
    pkgs.conmon
    pkgs.skopeo
    pkgs.slirp4netns
    pkgs.fuse-overlayfs
  ];
}
